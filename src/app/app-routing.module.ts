import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './page/login/login.component';
import { DashboardComponent } from './page/dashboard/dashboard.component';
import { AlumnosComponent } from './components/alumnos/alumnos.component';
import { InboxComponent } from './page/inbox/inbox.component';
import { CrudComponent } from './page/crud/crud.component';
import { FormStudentsComponent } from './page/form-students/form-students.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'alumnos', component: AlumnosComponent },
  { path: 'inbox', component: InboxComponent },
  { path: 'crud', component: CrudComponent },
  { path: 'formStudents', component: FormStudentsComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'login' }
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
