import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { IndexComponent } from './index/index.component';
import { AlumnosComponent } from './alumnos/alumnos.component';
import { ModulecontainerComponent } from './modulecontainer/modulecontainer.component';
import { SidemenumobileComponent } from './sidemenumobile/sidemenumobile.component';
import { TopbarComponent } from './topbar/topbar.component';
import { InboxComponent } from './inbox/inbox.component';



@NgModule({
  declarations: [SidemenuComponent, IndexComponent, AlumnosComponent, ModulecontainerComponent, SidemenumobileComponent, TopbarComponent, InboxComponent],
  exports: [SidemenuComponent, IndexComponent, AlumnosComponent],
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }
