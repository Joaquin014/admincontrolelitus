import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidemenumobileComponent } from './sidemenumobile.component';

describe('SidemenumobileComponent', () => {
  let component: SidemenumobileComponent;
  let fixture: ComponentFixture<SidemenumobileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidemenumobileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidemenumobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
