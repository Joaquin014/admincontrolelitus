import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css']
})
export class SidemenuComponent implements OnInit {
  
  ocultarMenu:string = "";
  fecha: string = "feather feather-chevron-down side-menu__sub-icon"
  ver: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  verMenu(){
    if(this.ver){
      this.ocultarMenu = "";
      this.fecha = "feather feather-chevron-down side-menu__sub-icon"
      this.ver = false;
    }else{
      this.ocultarMenu = "side-menu__sub-open";
      this.fecha = "feather feather-chevron-down side-menu__sub-icon transform rotate-180"
      this.ver = true;
    }
  }

}
