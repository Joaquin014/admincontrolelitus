import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './page/login/login.component';
import { ComponentsModule } from './components/components.module';
import { DashboardComponent } from './page/dashboard/dashboard.component';
import { InboxComponent } from './page/inbox/inbox.component';
import { CrudComponent } from './page/crud/crud.component';
import { FormStudentsComponent } from './page/form-students/form-students.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    InboxComponent,
    CrudComponent,
    FormStudentsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ComponentsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
